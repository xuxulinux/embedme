###### 本工程参照 external/opencv ######
LOCAL_PATH := $(call my-dir)

MY_CPP_STL_INCLUDES := 	\
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/include \
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/libs/armeabi/include
MY_CPP_STL_LIBS := \
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/libs/armeabi/libgnustl_static.a

MY_SQLITE3_INCLUDES := \
	$(TOP)/external/sqlite/dist

MY_CPP_CFLAGS := -DOS_ANDROID -frtti -fexceptions

#############################################
######  libcjson
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/android/cJSON
LOCAL_MODULE := libcjson

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
		$(MY_SRC_DIR)

LOCAL_SRC_FILES := 	\
	opensource/android/cJSON/cJSON.c

include $(BUILD_STATIC_LIBRARY)

#############################################
######  libconfig
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/android/libconfig-1.4.9
LOCAL_MODULE := libconfig

LOCAL_CFLAGS := -DLIBCONFIG_STATIC $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR) \
	$(MY_CPP_STL_INCLUDES)

LOCAL_SRC_FILES :=	\
	opensource/android/libconfig-1.4.9/grammar.c \
	opensource/android/libconfig-1.4.9/libconfig.c \
	opensource/android/libconfig-1.4.9/libconfigcpp.h \
	opensource/android/libconfig-1.4.9/libconfigcpp.cpp \
	opensource/android/libconfig-1.4.9/scanctx.c \
	opensource/android/libconfig-1.4.9/scanner.c \
	opensource/android/libconfig-1.4.9/strbuf.c

include $(BUILD_STATIC_LIBRARY)

#############################################
######  liblog4z
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/android/log4z
LOCAL_MODULE := liblog4z

LOCAL_CFLAGS := -DLIBCONFIG_STATIC $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR) \
	$(MY_CPP_STL_INCLUDES)
	
LOCAL_SRC_FILES := \
	opensource/android/log4z/log4z.cpp

include $(BUILD_STATIC_LIBRARY)

#############################################
######  libtinyalsa
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/tinyalsa
LOCAL_MODULE := libtinyalsa

LOCAL_CFLAGS := -DLIBCONFIG_STATIC $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR) \
	$(MY_CPP_STL_INCLUDES)
	
LOCAL_SRC_FILES := \
	opensource/tinyalsa/control.c \
	opensource/tinyalsa/mixer.c \
	opensource/tinyalsa/pcm.c
include $(BUILD_STATIC_LIBRARY)

#############################################
######  libemb
#############################################
include $(CLEAR_VARS)

MY_SRC_DIR := $(LOCAL_PATH)/libemb
LOCAL_MODULE := libemb

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson liblog4z
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/android/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/android/cJSON \
	$(MY_SRC_DIR)/../opensource/android/log4z \
	$(MY_SRC_DIR)/../opensource/tinyalsa \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	libemb/Array.cpp \
	libemb/BmpImage.cpp \
	libemb/CommandPipe.cpp \
	libemb/ComPort.cpp \
	libemb/Config.cpp \
	libemb/CRCCheck.cpp \
	libemb/Directory.cpp \
	libemb/Event.cpp \
	libemb/File.cpp \
	libemb/Gpio.cpp \
	libemb/HttpServer.cpp \
	libemb/IODevice.cpp \
	libemb/JSONData.cpp \
	libemb/KeyInput.cpp \
	libemb/KVProperty.cpp \
	libemb/Logger.cpp \
	libemb/MD5Check.cpp \
	libemb/LocalMsgQueue.cpp \
	libemb/Mutex.cpp \
	libemb/Network.cpp \
	libemb/RegExp.cpp \
	libemb/RtcFacade.cpp \
	libemb/Semaphore.cpp \
	libemb/Socket.cpp \
	libemb/SocketPair.cpp \
	libemb/SqliteWrapper.cpp \
	libemb/StringFilter.cpp \
	libemb/Thread.cpp \
	libemb/Timer.cpp \
	libemb/Tracer.cpp \
	libemb/TracerServer.cpp \
	libemb/Tuple.cpp \
	libemb/Utils.cpp \
	libemb/WavAudio.cpp

include $(BUILD_STATIC_LIBRARY)



#############################################
######  tracerclient
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := tracerclient

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES :=libemb libconfig libcjson liblog4z

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/android/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/android/cJSON \
	$(MY_SRC_DIR)/../opensource/android/log4z \
	$(MY_SRC_DIR)/../libemb

LOCAL_SRC_FILES := \
	app/tracerclient.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  test
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/test
LOCAL_MODULE := rcs-test

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libemb libconfig libcjson liblog4z libtinyalsa
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/android/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/android/cJSON \
	$(MY_SRC_DIR)/../opensource/android/log4z \
	$(MY_SRC_DIR)/../opensource/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../app \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	test/test.cpp \
	test/TestAny.cpp \
	test/TestArray.cpp \
	test/TestALSAAudio.cpp \
	test/TestCommandPipe.cpp \
	test/TestCom.cpp \
	test/TestConfig.cpp \
	test/TestDirectory.cpp \
	test/TestEvent.cpp \
	test/TestFBScreen.cpp \
	test/TestGpio.cpp \
	test/TestHttpServer.cpp \
	test/TestImage.cpp \
	test/TestJSON.cpp \
	test/TestKeyInput.cpp \
	test/TestKVProperty.cpp \
	test/TestLogger.cpp \
	test/TestLocalSocket.cpp \
	test/TestMain.cpp \
	test/TestMsgQueue.cpp \
	test/TestNetwork.cpp \
	test/TestOSSAudio.cpp \
	test/TestRegExp.cpp \
	test/TestRtcFacade.cpp \
	test/TestStringFilter.cpp \
	test/TestTcp.cpp \
	test/TestThread.cpp \
	test/TestThreadPool.cpp \
	test/TestTimer.cpp \
	test/TestTracer.cpp \
	test/TestTracerServer.cpp \
	test/TestTuple.cpp \
	test/TestUdp.cpp \
	test/TestUtils.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  配置文件预置
#############################################
$(warning force copy files:$(LOCAL_MODULE_PATH))
$(shell chmod 777 $(LOCAL_PATH)/test/test.conf)
$(shell cp -rf $(LOCAL_PATH)/test/test.conf $(LOCAL_MODULE_PATH)/)

