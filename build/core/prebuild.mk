##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
ifeq ($(LOCAL_PREBUILD_DST_PATH),)
LOCAL_PREBUILD_DST_PATH := $(DEFAULT_BIN_PATH)
endif

PREBUILD_FILES := $(foreach f,$(LOCAL_PREBUILD_SRC_FILES),$(SOURCES_PATH)/$(f))

.PHONY:all
all:
	@cp -pR $(PREBUILD_FILES) $(LOCAL_PREBUILD_DST_PATH) 
	@echo -e "\033[33m\033[1m==>prebuild successfully,file list:\033[0m"
	@for f in $(PREBUILD_FILES); do \
		echo -e "\033[33m\033[1m$$f\033[0m";done;

.PHONY:clean
clean:
	@rm -rf $(LOCAL_PREBUILD_DST_PATH)/$(LOCAL_PREBUILD_SRC_FILES)

