##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
ifeq ($(BUILD_SYSTEM),)
$(error "BUILD_SYSTEM is null.")
endif

ifeq ($(BUILD_OUTPUT_PATH),)
$(error "BUILD_OUTPUT_PATH is null.")
endif

ifeq ($(LOCAL_BUILD_STATIC_LIB),)
$(error "LOCAL_BUILD_STATIC_LIB is null.")
endif

ifeq ($(HOST),)
HOSTNAME := unknown
else
HOSTNAME := $(HOST)
endif

ifeq ($(LOCAL_BUILD_STATIC_LIB),yes)
LINKTYPE:=static
else
LINKTYPE:=shared
endif

.PHONY:all
all:
	@$(SOURCES_PATH)/build $(HOSTNAME) $(LINKTYPE) $(BUILD_OUTPUT_PATH) $(BUILD_SYSTEM) 
	@echo -e "\033[33m\033[1m==>build opensource successfully.\033[0m"
.PHONY: clean
clean:
	@$(SOURCES_PATH)/clean