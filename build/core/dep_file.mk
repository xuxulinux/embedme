##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
DEP_FILE := $(MODULE_FILE).dep
SOURCE_FILES := 
$(DEP_FILE):
	@echo "Generating new dependency file...";
	@-rm -f $(DEP_FILE)
	@for f in $(LOCAL_SRC_FILES); do \
	OBJ=$(OBJECT_PATH)/`basename $$f|sed -e 's/\.cpp/\.o/' -e 's/\.c/\.o/'`; \
        echo $$OBJ: $(SOURCES_PATH)/$$f>> $(DEP_FILE); \
        echo '	$(CC) $$(ALL_CC_FLAGS)  -c -o $$@ $$^'>> $(DEP_FILE); \
        done

include $(DEP_FILE)

# OBJECT_FILES:目标所依赖的所有.o文件
OBJECT_FILES:=$(addprefix $(OBJECT_PATH)/, $(addsuffix .o, $(basename $(notdir $(LOCAL_SRC_FILES)))))