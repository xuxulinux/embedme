##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
INC_PATHS := -I$(DEFAULT_INC_PATH)
INC_PATHS += $(foreach f,$(LOCAL_INC_PATHS),-I$(f))
LIB_PATHS := -L$(DEFAULT_LIB_PATH)
LIB_PATHS += $(foreach f,$(LOCAL_LIB_PATHS),-L$(f))

ALL_CC_FLAGS := $(INC_PATHS) $(LOCAL_CFLAGS) $(LIB_PATHS) $(LOCAL_LDFLAGS) $(DEFAULT_CC_FLAGS)

# MODULE_FILE:目标模块文件名
MODULE_FILE := $(OBJECT_PATH)/$(LOCAL_MODULE).a

.PHONY:all
all:$(MODULE_FILE)
	@cp -pR $(MODULE_FILE) $(DEFAULT_LIB_PATH)
	@echo -e "\033[33m\033[1m==>build $(LOCAL_MODULE).a in '$(SOURCES_DIR)' successfully.\033[0m"

# 生成编译单个.o文件所需的Makefile
include $(BUILD_DEPFILE)

# 生成目标文件
$(MODULE_FILE): $(OBJECT_FILES)
	$(AR) -r $@ $(OBJECT_FILES) 

.PHONY:clean
clean:
	rm -rf $(OBJECT_PATH)