## mbuild编译系统说明
build目录是mbuild编译系统的源码，build/envsetup.sh是编译环境设置脚本。在使用mbuild系统前，必须在工程根目录下执行：

**#source ./build/envsetup**

**执行完成后将可使用mbuild系统提供的命令。**

###命令说明

* **lunch --------lunch <hosttype\> 指定目标主机类型 (0:x86 1:arm-linux ...).**
* **mbuild 	------mbuild <directory\> <target\> 编译directory目录下的target目标.**
* **mclean 	------mclean <directory\> <target\> 清理directory目录下的target目标.**
* **mrball	-------编译整个工程**
* **mrprop  -------清理整个工程**

#####使用mbuild命令必须指定两个参数<directory\> <target\>
* **<directory\>  源码目录**
* **<target\>  源码目录下的目标名称，用户需编写对应的目标构建文件<target\>.mk**

---------------------------------------------------------------------
##目标构建文件target.mk说明
**mbuild系统支持构建以下几种目标**

* 可执行文件(BUILD\_EXECUTABLE)
* 静态链接库(BUILD\_STATIC\_LIBRARY)
* 动态链接库(BUILD\_SHARED\_LIBRARY)
* 部署文件(BUILD\_PREBUILD)

**注意：每个target.mk文件仅支持编译一个目标,每个目标需对应一个mk文件。**

**target.mk模板1(编译目标)**

LOCAL\_PATH := $(call my-dir) 

include $(CLEAR\_VARS)

LOCAL\_MODULE :=               

LOCAL\_CFLAGS :=

LOCAL\_LDFLAGS :=

LOCAL\_INC\_PATHS := $(LOCAL\_PATH)

LOCAL\_SRC\_FILES :=

include $(BUILD\_STATIC\_LIBRARY)

**target.mk模板2(部署文件)**

include $(CLEAR\_VARS)

LOCAL\_MODULE := config

LOCAL\_PREBUILD\_SRC\_FILES:= app.conf

LOCAL\_PREBUILD\_DST\_PATH := 

include $(BUILD\_PREBUILD)

**常用变量说明**

* LOCAL\_PATH 当前源码目录,固定写为LOCAL\_PATH := $(call my-dir) 
* CLEAR\_VARS 清除编译变量
* LOCAL\_MODULE 模块名称，最终生成的目标名称
* LOCAL\_CFLAGS 编译参数CFLAGS
* LOCAL\_LDFLAGS 链接参数LDFLAGS
* LOCAL\_INC\_PATHS 头文件搜索路径
* LOCAL\_LIB\_PATHS 链接库搜索路径
* LOCAL\_MODULE 模块名称，最终生成的目标名称
* LOCAL\_PREBUILD\_SRC\_FILES  部署源文件
* LOCAL\_PREBUILD\_DST\_PATH 部署文件的目标路径，默认部署到output/bin目录下

##工程样例
***假定在工程目录下有hello目录，目录下有源码hello.h，hello.c，main.c。我们的目标是生成一个libhello.a和一个可执行程序hello.***

**1. 在hello目录下新建libhello.a目标的构建文件libhello.mk:**

LOCAL\_PATH := $(call my-dir)

include $(CLEAR\_VARS)

LOCAL\_MODULE := libhello

LOCAL\_CFLAGS :=

LOCAL\_LDFLAGS :=

LOCAL\_INC\_PATHS := $(LOCAL\_PATH)

LOCAL\_SRC\_FILES := hello.c

include $(BUILD\_STATIC\_LIBRARY)

**2. 在hello目录下新建hello目标的构建文件hello.mk:**

LOCAL\_PATH := $(call my-dir)

include $(CLEAR\_VARS)

LOCAL\_MODULE := hello

LOCAL\_CFLAGS :=

LOCAL\_LDFLAGS := -lpthread

LOCAL\_INC_PATHS := $(LOCAL\_PATH)

LOCAL\_SRC\_FILES := main.cpp hello.c

include $(BUILD\_EXECUTABLE)

**3. 设置编译环境:**

*#source ./build/envsetup.sh*

*#lunch*

**4. 编译目标:**

*#mbuild hello libhello*

*#mbuild hello hello*

**5. 在output目录的lib和bin文件夹下你将发现编译出的目标文件:**

*#ls output/bin*

*hello*

*#ls output/lib*

*libhello.a*
