#!/bin/sh
##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
PROJECT_ROOT=$PWD
HOST=
BUILD_SYSTEM=`uname -o | tr '[A-Z]' 'a-z'`;
if [[ $BUILD_SYSTEM =~ "cygwin" ]];then
	BUILD_SYSTEM=OS_CYGWIN
else
	BUILD_SYSTEM=OS_UNIX
fi

if [ -d "$PROJECT_ROOT/build/core" -a -f "$PROJECT_ROOT/build/envsetup.sh" -a -f "$PROJECT_ROOT/build/core/main.mk" ]; then
#USERNAME=`whoami`
#newgrp $USERNAME
echo -e "\033[33m\033"
cat <<EOF
-------------------------------------------------------------------------------------------------
  [ mbuild system help ] 
  ----------------------
  Invoke ". build/envsetup.sh" from your shell to add the following functions to your environment:
- lunch :   lunch <hosttype> specify the host type (0:x86 1:arm-linux ...).
- mbuild:   mbuild <directory> <target> make target from the directory.
- mclean:   mclean <directory> <target> make clean target from the directory.
- mrball:   build all
- mrprop:   clean all
  --------------------------------------------------------------------------------
  Commands only can work well once you hava a <target>.mk file in the <directory>.
-------------------------------------------------------------------------------------------------
EOF
echo -e "\033[0m"
else
	echo -e '\033[33m\033 You should invoke ". build/envsetup.sh" in project top directory!'
	echo -e "come back to $HOME\033[0m"
	exit 1
fi
################################################################
## 以下函数是核心基础函数,请谨慎修改 !!!
################################################################
function sethost()
{
	case $1 in
	"1") 
		HOST=arm-linux;;
	"2")
		HOST=arm-none-linux-gnueabi;;
	*) 
		HOST=;;
	esac
}

function lunch()
{
	if [ $# = 0 ];then
		echo "lunch host:"
		echo "0: x86-linux/cygwin(default)"
		echo "1: arm-linux"
		echo "2: arm-none-linux-gnueabi"
		read n
		sethost $n
	else
		sethost $1
	fi
}

function mbuild()
{
	if [ $# = 1 -a $1 = "all" ];then
		mrball
		return
	elif [ $# != 2 ];then
		echo "[usage] mbuild <directory> <target>"
		return
	fi
	echo "build for HOST:$HOST"
	make clean -f build/core/main.mk SOURCES_DIR=$1 TARGET_NAME=$2 PROJECT_ROOT=$PROJECT_ROOT HOST=$HOST
    make all -f build/core/main.mk SOURCES_DIR=$1 TARGET_NAME=$2 PROJECT_ROOT=$PROJECT_ROOT HOST=$HOST
    if [ "$?" -ne 0 ]; then
    	while true
    	do
    		echo -e "\033[31m\033 Error occur on mbuild,press Ctrl+C to quit!\033[0m";
    		read n
    	done	
    fi
}

function mclean()
{
	if [ $# = 1 -a $1 = "all" ];then
		mrprop
		return
	elif [ $# != 2 ];then
		echo "[usage] mclean <directory> <target>"
		return
	fi
	make clean -f build/core/main.mk SOURCES_DIR=$1 TARGET_NAME=$2 PROJECT_ROOT=$PROJECT_ROOT HOST=$HOST
}

function mrball()
{
	mbuild opensource opensource
	mupdate
}

function mrprop()
{
	echo "Are you sure to clean the output:"
	echo "$PROJECT_ROOT/output/"
	echo 'please just say "yes" or "no":'
	read a
	case $a in
	"yes") 
		mclean opensource opensource
		/bin/rm -rf $PROJECT_ROOT/output/*
		echo "clean all finished.";;
	*) 
		echo "clean do nothing."
		return;;
	esac
	
}
################################################################
## 以下函数是用户自定义函数,请酌情修改 !!!
################################################################
function mupdate()
{
	mclean libemb libemb
	mbuild libemb libemb

	mclean app hello
	mbuild app hello
	
	mclean app tracerclient
	mbuild app tracerclient

	mclean test test
	mbuild test test

	mclean test test-prebuild
	mbuild test test-prebuild
}

function mtest()
{
	mclean test test
	mbuild test test

	mclean test test-prebuild
	mbuild test test-prebuild
	
	mclean app tracerclient
	mbuild app tracerclient
}

function mdeploy()
{
	if [ $# != 1 ];then
		echo "[usage] mdeploy <app_name>"
		return
	fi
	/bin/rm -rf $PROJECT_ROOT/output/$1
	mkdir -p $PROJECT_ROOT/output/$1
	mkdir -p $PROJECT_ROOT/output/$1/lib
	cp -pR $PROJECT_ROOT/output/bin/rcs $PROJECT_ROOT/output/$1
	cp -pR $PROJECT_ROOT/output/bin/rcs-boost $PROJECT_ROOT/output/$1
	cp -pR $PROJECT_ROOT/output/bin/rcs-test $PROJECT_ROOT/output/$1
	cp -pR $PROJECT_ROOT/output/bin/tracerclient $PROJECT_ROOT/output/$1
	cp -pR $PROJECT_ROOT/output/bin/rcs.conf $PROJECT_ROOT/output/$1
	cp -pR $PROJECT_ROOT/output/bin/startrcs $PROJECT_ROOT/output/$1
	if [ $BUILD_SYSTEM = "OS_CYGWIN" ];then
		cp -pR $PROJECT_ROOT/output/bin/*.dll* $PROJECT_ROOT/output/$1/lib/
	else
		cp -pR $PROJECT_ROOT/output/lib/*.so $PROJECT_ROOT/output/$1/lib/
	fi
	echo -e "\033[33m\033[1mdeploy $1 ok, you can find it in: "
	echo -e "$PROJECT_ROOT/output/$1\033[0m"
}
