#ifndef __SOCKETPAIR_H__
#define __SOCKETPAIR_H__
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

typedef enum{
    PAIR_FD_READ=0,
    PAIR_FD_WRITE,
    PAIR_FD_MAX
}PAIR_FD_E;

class SocketPair{
public:
    SocketPair();
    ~SocketPair();
    bool createPair(int& readFd,int& writeFd);
    bool bindPair(int readFd,int writeFd);
    int readData(char* buf,int len,int timeoutMs=-1);
    int writeData(const char* buf,int len, int timeoutMs=-1);
private:
    int m_fd[PAIR_FD_MAX];
};

#endif
