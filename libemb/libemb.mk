LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libemb

LOCAL_CFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	Array.cpp \
	BmpImage.cpp \
	CommandPipe.cpp \
	ComPort.cpp \
	Config.cpp \
	CRCCheck.cpp \
	Directory.cpp \
	Event.cpp \
	Fifo.cpp \
	File.cpp \
	Gpio.cpp \
	HttpClient.cpp \
	HttpServer.cpp \
	IODevice.cpp \
	JSONData.cpp \
	KeyInput.cpp \
	KVProperty.cpp \
	LocalMsgQueue.cpp \
	Logger.cpp \
	MD5Check.cpp \
	MemShared.cpp \
	MsgQueue.cpp \
	Mutex.cpp \
	Network.cpp \
	OssAudio.cpp \
	RegExp.cpp \
	RtcFacade.cpp \
	Semaphore.cpp \
	Socket.cpp \
	SocketPair.cpp \
	SqliteWrapper.cpp \
	StringFilter.cpp \
	Thread.cpp \
	Timer.cpp \
	Tracer.cpp \
	TracerServer.cpp \
	Tuple.cpp \
	Utils.cpp \
	WavAudio.cpp \
	XmlData.cpp

include $(BUILD_STATIC_LIBRARY)