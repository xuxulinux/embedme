/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2017 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BMPIMAGE_H__
#define __BMPIMAGE_H__

#include <iostream>

typedef enum{
    IMAGE_RAW_FORMAT_RGB565=0,
    IMAGE_RAW_FORMAT_RGB1555,
    IMAGE_RAW_FORMAT_RGB888,
}IMAGE_RAW_FORMAT_E;

/* 小端存储方式：0x1234 --存储为34 12 */
#pragma pack(1)    /* 紧凑对齐 */
typedef struct {
    char m_type[2];         /* 0x00-0x01,文件类型:"BM","BA" */
    uint32 m_size;          /* 0x02-0x05,文件大小 */
    uint16 m_reserved1;     /* 0x06-0x07,保留位,必须设置为0 */
    uint16 m_reserved2;     /* 0x08-0x09,保留位,必须设置为0 */
    uint32 m_offBits;       /* 0x0A-0x0D 图像数据偏移地址(54+n)*/
    uint32 m_headerSize;    /* 0x0E-0x11,位图信息头大小---头信息开始(共40字节) */
    uint32 m_width;         /* 0x12-0x15,图像宽度,像素 */
    uint32 m_height;        /* 0x16-0x19,图像高度 */
    uint16 m_flames;        /* 0x1A-0x1B,颜色平面数,总为1 */
    uint16 m_bits;          /* 0x1C-0x1D,像素位数:1,4,8,16,24,32 */
    uint32 m_compression;   /* 0x1E-0x21,压缩类型:0不压缩 */
    uint32 m_sizeImage;     /* 0x22-0x25,图像大小,压缩类型为0时可为0 */
    uint32 m_xppm;          /* 0x26-0x29,水平分辨率 */
    uint32 m_yppm;          /* 0x2A-0x2D,垂直分辨率 */
    uint32 m_colorIndex;    /* 0x2E-0x31,颜色索引数,0表示使用所有颜色 */
    uint32 m_colorImportant;/* 0x32-0x35,重要的颜色索引数,0表示都重要 */
}BmpHeader_S;
#pragma pack()

/**
 *  \file   BmpImage.h
 *  \class  BmpImage
 *  \brief  BMP图像类
 */
class BmpImage{
public:
    BmpImage();
    ~BmpImage();
    bool loadFormFile(std::string fileName);
    bool loadFromRawData(int width,int height,int format,const char* rawData,int size);
    bool saveAsFile(std::string fileName);
    int getWidth();
    int getHeight();
private:
    char* m_data;
    int m_dataLen;
    BmpHeader_S m_header;
};

#endif

