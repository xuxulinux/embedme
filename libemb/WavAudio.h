/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2017 @ ShenZhen ,China
*******************************************************************************/
#ifndef __WAV_AUDIO_H__
#define __WAV_AUDIO_H__

#include <iostream>

class WavAudio{
public:
    WavAudio();
    ~WavAudio();
    bool startPlay(std::string wavFileName,int device=0, int card=0,
                   int periodSize=1024, int periodCount=4);
    bool stopPlay();
    bool startRecord(std::string wavFileName,int rates,int bits,int device=0,
                     int card=0,int channels=2,int periodSize=1024, int periodCount=4);
    bool stopRecord();
private:
    bool m_startPlaying;
    bool m_startRecording;
};
#endif
