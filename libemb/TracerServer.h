#ifndef __TRACER_SERVER_H__
#define __TRACER_SERVER_H__

#include "Thread.h"
#include "Socket.h"
#include <iostream>

class TracerServer : public LocalTcpServer{
public:
    ~TracerServer();
    static TracerServer* getInstance()
    {
        static TracerServer instance;
        return &instance;
    }
private:
    TracerServer();
    void onNewConnection(Socket* connSocket); 
    bool attachTracer(); 
    bool disattachTracer();
private:
    std::string m_ptsDev;
    bool m_enable;
};

#endif
