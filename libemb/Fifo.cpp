#include "Tracer.h"
#include "Fifo.h"
#include <sys/types.h>
#include <sys/stat.h>

Fifo::Fifo()
{
}

Fifo::~Fifo()
{
}

bool Fifo::open(const char* devName,int ioMode)
{
    if (devName==NULL)
    {
        TRACE_ERR_CLASS("fifo name is NULL.\n");
        return false;
    }
    if(!File::isExist(devName))
    {
        int ret=mkfifo(devName,S_IFIFO|0666);
        if (ret<0) 
        {
            TRACE_ERR_CLASS("mkfifo error:%s.\n", ERROR_STRING); 
            return false;
        }
    }
    return m_file.open(devName,ioMode);
}

bool Fifo::close()
{
    return m_file.close();
}

int Fifo::readData(char * buf, int len, int timeoutMs)
{
    return m_file.readData(buf,len);
}
int Fifo::writeData(const char *buf, int len, int timeoutMs)
{
    return m_file.writeData(buf,len);
}


