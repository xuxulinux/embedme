﻿/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2017 @ ShenZhen ,China
*******************************************************************************/
#include "Directory.h"
#include "File.h"
#include "Tracer.h"
#include "Utils.h"
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <algorithm>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>

Directory::Directory():
m_currentPath(".")
{
}

Directory::~Directory()
{
}

/**
 *  \brief  判断目录是否为空
 *  \param  name 目录全名(如:/usr/share)
 *  \return 目录为空或不存在返回true,不为空则返回false
 *  \note   none
 */
bool Directory::isEmpty(const char* name)
{
	DIR * dirp = NULL;
    struct dirent *dp=NULL;
    int num=0;
    dirp = opendir(name);
    if (NULL==dirp)
    {
		//TRACE_ERR("Directory::isDirEmpty open dir:%s error(%s).\n",name,ERROR_STRING);
		return true;
    }
    while((dp=readdir(dirp))!=NULL)
    {
        if (strcmp(dp->d_name,".")==0 ||
            strcmp(dp->d_name,"..")==0)
        {
            continue;
        }
        else
        {
            num++;
            break;
        }
    }
    closedir(dirp);
    if(num>0)
    {
        return false;
    }
	return true;
}

/**
 *  \brief  判断目录是否存在
 *  \param  name 目录全名(如:/usr/share)
 *  \return 目录存在返回true,不存在则返回false
 *  \note   只有name是目录名称时才返回true,如果是文件名则返回false
 */
bool Directory::isExist(const char* name)
{
    if (name==NULL) 
    {
        return false;
    }
    if (0==access(name,F_OK))
    {
        struct stat fileStat;
        if (0!=lstat(name,&fileStat))
        {
            return false;
        } 
        else 
        {
            if (S_ISDIR(fileStat.st_mode))
            {
                return true;
            }
            if (S_ISLNK(fileStat.st_mode))
            {
                DIR* dirp = opendir(name);
                if (dirp!=NULL) 
                {
                    closedir(dirp);
                    return true;
                }
            }
        }
    }
	return false;
}

/**
 *  \brief  获取文件夹内容大小
 *  \param  name 文件夹名
 *  \return 返回文件夹的大小，以byte为单位
 *  \note   none
 */
int Directory::getContentSize(const char* name)
{
    return File::getContentSize(name);
}

/**
 *  \brief  创建文件夹
 *  \param  name 文件夹名
 *  \param  recursice 是否递归
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool Directory::createDir(const char* name,bool recursive)
{
    if (name==NULL)
        return false;
    if (recursive) {
        vector<string> dirNames = Utils::splitString(name, "/",true);
        if (!dirNames.empty()) {
            string dirPath="";
            for (int i=0; i<dirNames.size();i++) {
                if (dirNames[i]=="/") {
                    if (i==0)
                        dirPath += "/";
                } else {
                    dirPath += dirNames[i];
                    dirPath += "/";
                    if (!Directory::isExist(dirPath.c_str())) {
                        if (mkdir(dirPath.c_str(), 0644) != 0) {
                            TRACE_ERR("Directory::createDir(%s) error:%s.\n", dirPath.c_str(), ERROR_STRING);
                            return false;
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    } else {
        if (mkdir(name, 0644) != 0){
            TRACE_ERR("Directory::createDir(%s) error:%s.\n", name, ERROR_STRING);
            return false;
        } else {
            return true;
        }
    }
}
/**
 *  \brief  删除文件夹
 *  \param  name 文件夹名
 *  \param  recursice 是否递归
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool Directory::deleteDir(const char* name,bool recursive)
{
    if (name==NULL)
        return false;
    if (recursive) {
        vector<string> dirNames = Utils::splitString(name, "/",true);
        if (!dirNames.empty()) {
            string dirPath="";
            for (int i=0; i<dirNames.size();i++) {
                if (dirNames[i]=="/") {
                    if (i==0 || (i==(dirNames.size()-1))) {
                        dirPath += "/";
                    }
                } else {
                    dirPath += dirNames[i];
                    dirPath += "/";
                }
            }
            if (Directory::isExist(dirPath.c_str())) {
                if (!Directory::isEmpty(dirPath.c_str())) {/* 先删除该目录下的所有子目录和文件 */
                    Directory dir;
                    dir.enter(dirPath.c_str());
                    vector<string> allFiles=dir.listAll();
                    for (int i=0; i<allFiles.size();i++) {
                        if (allFiles[i]=="." || allFiles[i]=="..") {
                            continue;
                        } else {
                            string fileName = dirPath + allFiles[i];
                            if (Directory::isExist(fileName.c_str())) {
                                if(!deleteDir(fileName.c_str(),true)){
                                    TRACE_ERR("Directory::deleteDir,del1 dir(%s) error:%s.\n", fileName.c_str(), ERROR_STRING);
                                    return false;
                                }
                            } else {
                                if (!File::deleteFile(fileName.c_str())) {
                                     TRACE_ERR("Directory::deleteDir,del1 file(%s) error:%s.\n", fileName.c_str(), ERROR_STRING);
                                     return false;
                                 }
                            }
                        }
                    }
                }
            }
            return deleteDir(dirPath.c_str());/* 删除目录 */
        }
        return false;
    } else {
        if (remove(name)!=0) {
            TRACE_ERR("Directory::deleteDir(%s) error:%s.\n", name, ERROR_STRING);
            return false;
        }
        return true;
    }
}

/**
 *  \brief  进入目录
 *  \param  path 目录全名(如:/usr/share)
 *  \return 成功返回true，失败返回false
 *  \note   none
 */
bool Directory::enter(const string & path)
{
    string resolved;
    if (path.empty())
    {
        return false;
    }
    if (path[0]!='/')
    {
        resolved = m_currentPath+"/"+path;
    }
    else
    {
        resolved = path;
    }
    DIR* dirp;
    struct dirent * dp = NULL;
    dirp = opendir(resolved.c_str());
    if (NULL == dirp)
    {
        return false;
    }
    closedir(dirp);
    char buf[1024]={0};
    if (NULL==realpath(resolved.c_str(),buf))
    {
        return false;
    }
    m_currentPath = string(buf);
    return true;
}

/**
 *  \brief  获取当前路径
 *  \param  none
 *  \return 返回绝对路径
 *  \note   none
 */
string Directory::current()
{
    return m_currentPath;
}

/**
 *  \brief  获取目录下所有文件
 *  \param  none
 *  \return 返回所有文件名
 *  \note   none
 */
vector<string> Directory::listAll()
{
    vector<string> fileVector;
    std::string fileName;
    DIR* dirp = opendir(m_currentPath.c_str());
    if (NULL == dirp)
    {
        return fileVector;
    }
    #if 0
    struct dirent *dp1 = (struct dirent*)malloc(sizeof(struct dirent));
    struct dirent *dp2 = (struct dirent*)malloc(sizeof(struct dirent));
    while ((readdir_r(dirp,dp1,&dp2))!=0)
    {
        fileName = string(dp2->d_name);
        fileVector.push_back(fileName);
    }
    closedir(dirp);
    free(dp1);
    free(dp2);
    #else
    struct dirent *dp;
    while ((dp=readdir(dirp))!=NULL)
    {
        fileName = string(dp->d_name);
        fileVector.push_back(fileName);
    }
    closedir(dirp);
    #endif
    sort(fileVector.begin(),fileVector.end());
    return fileVector;
}
