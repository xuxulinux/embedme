﻿/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2017 @ ShenZhen ,China
*******************************************************************************/
#ifndef __ATOMICVARIABLE_H__
#define __ATOMICVARIABLE_H__

/** 
 *  \file  AtomicVariable.h
 *  \brief 原子变量头文件
 *  原子变量的模板实现
 */

#include "Mutex.h"
template <class Type>
/**
 *  \file   AtomicVariable.h   
 *  \class  AtomicVariable
 *  \brief  原子变量量.
 */
class AtomicVariable{
public:
    AtomicVariable();
    ~AtomicVariable();
    Type getValue(void);
    void setValue(Type& value);
    bool testValue(Type& value);
private:
    MutexLock m_variableMutex;
    Type m_value;
};

template <class Type>
AtomicVariable<Type>::AtomicVariable()
{}

template <class Type>
AtomicVariable<Type>::~AtomicVariable()
{}


template <class Type>
Type AtomicVariable<Type>::getValue()
{
    AutoLock lock(&m_variableMutex);
    return m_value;
}

template <class Type>
void AtomicVariable<Type>::setValue(Type& value)
{
    AutoLock lock(&m_variableMutex);
    m_value = value;
}

template <class Type>
bool AtomicVariable<Type>::testValue(Type& value)
{
    AutoLock lock(&m_variableMutex);
    return (m_value==value)?true:false;
}

#endif
