#include "IODevice.h"
#include "File.h"

class Fifo:public IODevice{
public:
    Fifo();
    virtual ~Fifo();
    virtual bool open(const char* devName,int ioMode);
    virtual bool close();
    virtual int readData(char * buf, int len, int timeoutMs=-1);
    virtual int writeData(const char *buf, int len, int timeoutMs=-1);
private:
    File m_file;
};


