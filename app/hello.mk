LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := hello

LOCAL_CFLAGS :=
LOCAL_LDFLAGS := -lemb -lpthread -lrt -lsqlite3 -ldl -ltinyxml -lcjson -lconfig++ -llog4z
LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../libemb

LOCAL_SRC_FILES := \
	hello.cpp

include $(BUILD_EXECUTABLE)