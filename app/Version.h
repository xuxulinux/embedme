﻿/**********************************README***********************************
 * This file is part of app of rcs  (romote control service) project.
 * 用来存储软件版本号，比如这里表示软件版本是： V2.00.02.002
 * 暂时没有用到，可以保留；
*******************************************************************************/
#ifndef __VERSION_H__
#define __VERSION_H__

#include "BaseType.h"

typedef struct
{
    uint8 v1_main; /* 大版本号 */
    uint8 v2_sub;  /* 小版本号 */
    uint8 v3_test; /* 测试次数 */
    uint8 v4_sum;  /* 版本总数 */
}SWVERSION_S;

static SWVERSION_S SWVER = {2, 0, 2, 2};

#endif
