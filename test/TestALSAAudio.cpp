#include "Tracer.h"
#include "WavAudio.h"
#include "File.h"
#include <stdlib.h>
#include <iostream>

#ifdef OS_CYGWIN
void TestALSAAudio()
{
    TRACE_ERR("Not support WavAudio Test.\n");
}
#else

class WavPlayer:public Runnable{
public:
    void startPlay(std::string fileName)
    {
        Thread::createTask(this, task_selector(WavPlayer::playTask),(void*)&fileName);
    }
    void stopPlay()
    {
        m_player.stopPlay();
    }
private:
    void run(){};
    void playTask(void* args)
    {
        m_player.startPlay(*((std::string*)args));
        TRACE_WARN_CLASS("--->>> play stoped,file name:%s.\n",(*((std::string*)args)).c_str());
    }
private:
    WavAudio m_player;
};

class WavRecorder:public Runnable{
public:
    void startRecord(std::string fileName)
    {
        Thread::createTask(this, task_selector(WavRecorder::recordTask),(void*)&fileName);
    }
    void stopRecord()
    {
        m_recorder.stopRecord();
    }
private:
    void run(){};
    void recordTask(void* args)
    {
        m_recorder.startRecord(*((std::string*)args),8000,16);
        TRACE_WARN_CLASS("--->>> record stoped,file name:%s.\n",(*((std::string*)args)).c_str());
    }
private:
    WavAudio m_recorder;
};

void TestALSAAudio()
{
    WavPlayer player;
    WavRecorder recorder;
    std::string inputStr;
	while(1)
	{
		TRACE_YELLOW("q ---> quit.\n");
        TRACE_YELLOW("1 ---> play wav.\n");
        TRACE_YELLOW("2 ---> stop play.\n");
        TRACE_YELLOW("3 ---> record wav.\n");
        TRACE_YELLOW("4 ---> stop record.\n");
		std::cin >> inputStr;
        std::cout << inputStr<<std::endl;
		if("q"==inputStr) 
		{
			break;
		}
        else if("1"==inputStr)
        {
            TRACE_YELLOW("play file name:\n");
            std::cin >> inputStr;
            std::cout << inputStr<<std::endl;
            TRACE_YELLOW("start playing.\n");
            player.startPlay(inputStr);
        }
        else if("2"==inputStr)
        {
            TRACE_YELLOW("stop playing.\n");
            player.stopPlay();
        }
        else if("3"==inputStr)
        {
            TRACE_YELLOW("record file name:\n");
            std::cin >> inputStr;
            std::cout << inputStr<<std::endl;
            TRACE_YELLOW("start recording.\n");
            recorder.startRecord(inputStr);
        }
        else if("4"==inputStr)
        {
            TRACE_YELLOW("stop recording.\n");
            recorder.stopRecord();
        }
    }
}
#endif
