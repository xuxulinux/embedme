#include "Tracer.h"
#include "BmpImage.h"
#include "File.h"
#include <stdlib.h>
#include <iostream>

void TestImage()
{
    std::string inputStr;
	while(1)
	{
		TRACE_YELLOW("q ---> quit.\n");
        TRACE_YELLOW("1 ---> load raw data(1024*768-RGB565).\n");
		std::cin >> inputStr;
        std::cout << inputStr<<std::endl;
		if("q"==inputStr) 
		{
			break;
		}
        else if("1"==inputStr)
        {
            BmpImage bmpImage;
            TRACE_YELLOW("input file name:\n");
            std::cin >> inputStr;
            std::cout << inputStr<<std::endl;
            File file;
            file.open("fb.raw", IO_MODE_RD_ONLY);
            char* buffer = (char*)malloc(1024*768*2);
            file.readData(buffer,1024*768*2);
            bmpImage.loadFromRawData(1024, 768, IMAGE_RAW_FORMAT_RGB565,buffer,1024*768*2);
            bmpImage.saveAsFile(inputStr);
            free(buffer);
        }
    }
}
