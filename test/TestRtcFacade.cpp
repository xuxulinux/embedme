#include "Tracer.h"
#include "RtcFacade.h"

#include <sys/time.h>

void TestRtcFacade(void)
{
    struct timeval tv;
    //tv.tv_sec;
    //tv.tv_usec;
    struct timezone tz;
    //tz.tz_minuteswest;
    //tz.tz_dsttime;
    int ret=gettimeofday(&tv,&tz);
    if (ret<0) {
        TRACE_ERR("gettimeofday error.\n");
    } else {
        TRACE_YELLOW("gettimeofday ok,tv(%d:%d),tz(%d:%d)\n",tv.tv_sec,tv.tv_usec,tz.tz_minuteswest,tz.tz_dsttime);
    }
    tv.tv_sec += 3600;
    ret=settimeofday(&tv,&tz);
    if (ret<0) {
        TRACE_ERR("settimeofday error.\n");
    } else {
        TRACE_YELLOW("settimeofday+3600s ok,tv(%d:%d)\n",tv.tv_sec,tv.tv_usec);
    }

    RtcTime_S rtcTime;
    TRACE_YELLOW("------read system time------\n");
    RtcFacade::getInstance()->readTime(rtcTime);
    RtcFacade::getInstance()->showTime(rtcTime);
    rtcTime.m_date = 1; 
    rtcTime.m_month = 1; 
    rtcTime.m_year = 2016; 
    rtcTime.m_hour = 8; 
    rtcTime.m_minute = 0; 
    rtcTime.m_second = 0;
    TRACE_YELLOW("------write time(2016-01-01 08:00:00)------\n");
    RtcFacade::getInstance()->writeTime(rtcTime);
    RtcFacade::getInstance()->showTime(rtcTime);

}
