﻿#include "BaseType.h"
#include "Tracer.h"
#include "Logger.h"
#include "Config.h"

/*

CoreLogger:
{
    logPath="./corelog";
    logFileName=".log";
    logFileNumMax=100;
};
DataLogger:
{
    logPath="./logdb";
    logFileName="logdb";
};
MainLogger:
{
    logPath="./mainlog";
    logFileSizeMax=10;
    logStoreMax=1024;
    monthDirEnable=1;
    loglist=(
    {
        name="sms";
        path="./sms/";
    },
    {
        name="call";
        path="./call/";
    });  
};
 
 
*/

static void testCoreLogger()
{
    Config config;
	config.initWithFile("logger.cfg");
	Settings logSettings = config["CoreLogger"];
	CoreLogger::getInstance()->initWithSettings(logSettings);
    Tracer::getInstance()->traceTimerBegin();
    for(int i=0;i<2000;i++) 
    {
        LOG_CORE("this is core logger pressure test......\n");
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();
}

static void testDataLogger()
{
    Config config;
	config.initWithFile("logger.cfg");
	Settings logSettings = config["DataLogger"];
	DataLogger::getInstance()->initWithSettings(logSettings);

    Tracer::getInstance()->traceTimerBegin();
    for(int i=0;i<20;i++) 
    {
        LOG_DATA(5,"this is datalogger pressure test......");
        Thread::msleep(1);
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();


}

static void testMainLogger()
{
    Config config;
	config.initWithFile("logger.cfg");
	Settings logSettings = config["MainLogger"];
	MainLogger::getInstance()->initWithSettings(logSettings);
    LOG_MAIN("main","this is main log.");
    LOG_MAIN("sms","this is sms log.");
    LOG_MAIN("call","this is call log.");

    /* log4z会有单独的线程进行日志写入,因此这里计算的时间并非实际日志写入时间 */
    Tracer::getInstance()->traceTimerBegin();
    for(int i=0;i<2000;i++) 
    {
        LOG_MAIN("main", "this is mainLog pressure test...i=%d...",i);
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();
}
static void testLoggerFacade()
{
    LoggerFacade* logger=LoggerFacade::getInstance();
    TRACE_YELLOW("-------------coreLog---------------\n");
    Tracer::getInstance()->traceTimerBegin();
    for (int i=0;i<2000; i++) 
    {
        logger->coreLog(Utils::stringFormat("This is core log: %04d.", i));
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();

    TRACE_YELLOW("-------------dataLog---------------\n");
    Tracer::getInstance()->traceTimerBegin();
    for (int i=0;i<20; i++) 
    {
        logger->dataLog(2,Utils::stringFormat("This is data log: %04d.", i));
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();

    TRACE_YELLOW("-------------mainLog---------------\n");
    Tracer::getInstance()->traceTimerBegin();
    for (int i=0;i<2000; i++) 
    {
        logger->mainLog("main",Utils::stringFormat("This is main log: %04d.", i));
    }
    Tracer::getInstance()->traceTimerEnd();
    Tracer::getInstance()->traceTimerDuration();
}
void TestLogger()
{
    while(1)
    {
        TRACE_DBG("------Logger test------\n");
        TRACE_DBG("1: test CoreLogger\n");
        TRACE_DBG("2: test DataLogger\n");
        TRACE_DBG("3: test MainLogger\n");
        TRACE_DBG("4: test LoggerFacade\n");
        TRACE_DBG("q:quit.\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        }
        else if ("1" == inputStr)
        {
            testCoreLogger();
        }
        else if ("2" == inputStr)
        {
            testDataLogger();
        }
        else if ("3" == inputStr)
        {
            testMainLogger();
        }
        else if ("4" == inputStr)
        {
            testLoggerFacade();
        }
        else
        {
            TRACE_DBG("Invalid command.\n");
        }
     }
	
    
    
}
