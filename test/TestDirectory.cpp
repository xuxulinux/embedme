﻿#include "Tracer.h"
#include "Directory.h"

void TestDirectory()
{
    std::string inputStr;
    Directory dir;
    while (1)
    {
        std::cout << "------Directory Test------" << std::endl;
        std::cout << "1. enter directory." << std::endl;
        std::cout << "2. current directory." << std::endl;
        std::cout << "3. list directory." << std::endl;
        std::cout << "4. create directory." << std::endl;
        std::cout << "5. delete directory." << std::endl;
        std::cout << "q/quit. quit test." << std::endl;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("1" == inputStr)
        {
            std::cout << "please input directory:";
            std::cin >> inputStr;
            if(!dir.enter(inputStr))
            {
                TRACE_RED("can't enter: %s\n",inputStr.c_str());
            }
        }
        else if ("2" == inputStr)
        {
            TRACE_YELLOW("current dir path: %s \n",dir.current().c_str());
        }
        else if ("3" == inputStr)
        {
            vector<string> fileList=dir.listAll();
            for(int i=0;i<fileList.size();i++)
            {
                if(Directory::isExist(fileList[i].c_str()))
                {
                    TRACE_CYAN("[%02d]dir :  %s\n",i,fileList[i].c_str());
                }
                else
                {
                    TRACE_YELLOW("[%02d]file:  %s\n",i,fileList[i].c_str());
                }
            }
        }
        else if ("4" == inputStr)
        {
            std::cout << "create dir name:";
            std::cin >> inputStr;
            if (!dir.createDir(inputStr.c_str(),true)) 
            {
                TRACE_RED("can't create dir: %s\n",inputStr.c_str());
            }
        }
        else if ("5" == inputStr)
        {
            std::cout << "delete dir name:";
            std::cin >> inputStr;
            if (!dir.deleteDir(inputStr.c_str(),true)) 
            {
                TRACE_RED("can't delete dir: %s\n",inputStr.c_str());
            }
        }
        else if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        } 
        else
        {
            ;
        }
        inputStr.clear();
    }
}