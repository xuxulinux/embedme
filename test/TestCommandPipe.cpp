﻿#include "Tracer.h"
#include "CommandPipe.h"
void TestCommandPipe()
{
    while (1) 
    {
        TRACE_YELLOW("-------CommandPipe test------.\n");
        TRACE_YELLOW("quit/q ---> quit test.\n");
        TRACE_YELLOW("test   ---> test.\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr)
        {
            break;
        }
        else if("test"==inputStr)
        {
            std::string result = CommandPipe::execute("ls -alh");
            printf("%s\n",result.c_str());
            continue;
        }
    }
}