#include "HttpServer.h"
#include "Tracer.h"
#include "BaseType.h"

#include <stdlib.h>

void TestHttpServer(void)
{
    HttpServer server;
    server.setRootPath("cgi-bin/");
    std::string inputStr;
    TRACE_YELLOW("config http server port:");
    std::cin >> inputStr;
    std::cout << inputStr << std::endl;
    uint16 port = atoi(inputStr.c_str());
    if(!server.initServer(port,"0.0.0.0"))
    {
        TRACE_ERR("http server init failed.\n");
        return;
    }
    while (1) 
    {
        TRACE_YELLOW("-------http server test------.\n");
        TRACE_YELLOW("quit/q ---> quit test.\n");
        TRACE_YELLOW("start  ---> start server.\n");
        TRACE_YELLOW("stop   ---> stop server.\n");
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr)
        {
            break;
        }
        else if("start"==inputStr)
        {
            server.startServer();
            continue;
        }
        else if("stop"==inputStr)
        {
            server.stopServer();
            continue;
        }
    }
    server.stopServer();
}
