﻿#include "Array.h"
#include "Tracer.h"
#include <iostream>
using namespace std;

void TestArray(void)
{
     while(1)
    {
        TRACE_DBG("Array Test --- q:quit.t:test\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        }
        else
        {
            IntArray intArray;
            intArray.initWithString("[1,2,3,4,5]");
            intArray<<6<<7<<8<<9<<10;
            cout<<intArray.serialize()<<endl;

            IntArray intArray2("[11,12,13,14,15]");
            intArray2<<16<<17<<18<<19<<20;
            cout<<intArray2.serialize()<<endl;

            StringArray stringArray;
            stringArray.initWithString("[\"One\",\"Two\",\"Three\",\"Four\",\"Five\"]");
            stringArray<<"Six"<<"Seven"<<"Eight"<<"Nine"<<"Ten";
            cout<<stringArray.serialize()<<endl;

            StringArray stringArray2("[\"One\",\"Two\",\"Three\",\"Four\",\"Five\"]");
            stringArray2<<"Six"<<"Seven"<<"Eight"<<"Nine"<<"Ten";
            cout<<stringArray2.serialize()<<endl;       
        }
    }
}