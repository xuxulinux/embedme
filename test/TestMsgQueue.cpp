﻿#include "Thread.h"
#include "Tracer.h"
#ifdef OS_ANDROID
#include "LocalMsgQueue.h"
#else
#include "MsgQueue.h"
#endif

#define MQ_ID	0x1234

class MsgSender:public Runnable{
public:
	void run()
	{
        #ifdef OS_ANDROID
        LocalMsgQueue *mq = LocalMsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
        #else
		MsgQueue *mq = MsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
        #endif
		while(1)
		{
			QueueMsg_S msg={2,5,"hello"};
			mq->sendMsg(msg);
			sleep(2);
		}
	}
};

class MsgReciever:public Runnable{
public:
	void run()
	{
		#ifdef OS_ANDROID
        LocalMsgQueue *mq = LocalMsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
        #else
		MsgQueue *mq = MsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
        #endif
		while(1)
		{
			QueueMsg_S msg={0};
			if(STATUS_OK==mq->recvMsg(msg,2))
			{
			    TRACE_DBG_CLASS("Recieved Message type:%d,len=%d, content:%s.\n",msg.m_msgType,msg.m_dataLen,msg.m_data);
            }
		}
	}
};

void TestMsgQueue()
{
	Thread sendThread,recvThread;
	MsgSender sender;
	MsgReciever recver;
	sendThread.start(&sender);
	recvThread.start(&recver);
	while(1)
	{
		TRACE_DBG("MsgQueue Test is running----------\n");
		sleep(5);
	}
}
