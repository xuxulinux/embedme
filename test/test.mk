LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := test

LOCAL_CFLAGS :=
LOCAL_LDFLAGS := -lemb -lpthread -lrt -lsqlite3 -ldl -ltinyxml -lcjson -lconfig++ -llog4z -ltinyalsa

ifeq ($(BUILD_SYSTEM),OS_CYGWIN)
$(warning "cygwin can't use libcurl!")
else
LOCAL_LDFLAGS += -lcurl
endif

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../libemb \
	$(LOCAL_PATH)/../app

LOCAL_SRC_FILES := \
	test.cpp \
	TestALSAAudio.cpp \
	TestAny.cpp \
	TestArray.cpp \
	TestCommandPipe.cpp \
	TestCom.cpp \
	TestConfig.cpp \
	TestDirectory.cpp \
	TestEvent.cpp \
	TestFBScreen.cpp \
	TestGpio.cpp \
	TestHttpClient.cpp \
	TestHttpServer.cpp \
	TestImage.cpp \
	TestJSON.cpp \
	TestKeyInput.cpp \
	TestKVProperty.cpp \
	TestLogger.cpp \
	TestLocalSocket.cpp \
	TestMain.cpp \
	TestMsgQueue.cpp \
	TestNetwork.cpp \
	TestOSSAudio.cpp \
	TestRegExp.cpp \
	TestRtcFacade.cpp \
	TestStringFilter.cpp \
	TestTcp.cpp \
	TestThread.cpp \
	TestThreadPool.cpp \
	TestTimer.cpp \
	TestTracer.cpp \
	TestTracerServer.cpp \
	TestTuple.cpp \
	TestUdp.cpp \
	TestUtils.cpp

include $(BUILD_EXECUTABLE)