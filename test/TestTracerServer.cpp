﻿#include "Tracer.h"
#include "TracerServer.h"
#include <iostream>

#define REMOTE_SOCKET_NAME  "rcs.socket.remotetracer"

void TestTracerServer(void)
{
    TracerServer* server=TracerServer::getInstance(); 
    server->initServer(REMOTE_SOCKET_NAME);
    server->startServer();

    while (1)
    {
        std::cout<<"------------------"<<std::endl;
        TRACE_DBG("TRACE_DBG.\n");
        TRACE_YELLOW("TRACE_YELLOW.\n");
        TRACE_RED("TRACE_RED.\n");
        sleep(1);
    }
}