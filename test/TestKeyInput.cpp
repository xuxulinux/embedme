﻿#include "KeyInput.h"
#include "Tracer.h"
#ifdef OS_CYGWIN
void TestKeyInput()
{
    TRACE_ERR("Not support KeyInput!\n");
}
#else
class MyKeyListener:public KeyListener{
public:
    void handleKey(int keycode)
    {
        TRACE_PINK("Recieved Key Event:");
        string event;
        switch(keycode)
        {
            case KEY_UP: event = "KEY_UP";break;
            case KEY_DOWN: event = "KEY_DOWN";break;
            case KEY_LEFT: event = "KEY_LEFT";break;
            case KEY_RIGHT: event = "KEY_RIGHT";break;
            case KEY_F1: event = "KEY_OK";break;
            case KEY_F2: event = "KEY_CALL";break;
            case KEY_F3: event = "KEY_HANGUP";break;
            default: event = "unknown key";break;
        }
        TRACE_PINK("%s,code=%d\n",event.c_str(),keycode);
    }
};


#define INPUT_DEVICE   "/dev/event0"

void TestKeyInput()
{
    std::string inputStr;
    std::cout << "please input key event device:" << std::endl;
    std::cin >> inputStr;
    std::cout << inputStr << std::endl;
    if (inputStr.empty()) 
    {
        inputStr = INPUT_DEVICE;
    }

    MyKeyListener listener;
    KeyInput* keyInput=KeyInput::getInstance();
    keyInput->addKeyListener(&listener);

    if(!keyInput->open(inputStr))
    {
        TRACE_ERR("open device failed:%s\n",inputStr.c_str());
        return;
    }
    while(1)
    {
        TRACE_DBG("Test KeyInput Process is running, \"q\" or \"quit\" for exit.\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            keyInput->close();
            keyInput->delKeyListener(&listener);
            return;
        }
        inputStr.clear();
    }
}
#endif
