﻿#include "Tracer.h"
#include <iostream>
extern void TestAny(void);
extern void TestUtils(void);
extern void TestCommandPipe();
extern void TestMsgQueue();
extern void TestEvent();
extern void TestThread();
extern void TestTimer(void);
extern void TestTcp(void);
extern void TestUdp(void);
extern void TestKVProperty();
extern void TestComPort();
extern void TestNetwork();
extern void TestDataBase();
extern void TestTask();
//extern void TestHttpClient();
extern void TestJSON(void);
extern void TestCTimer(void);
extern void TestXml(void);
extern void TestStringFilter(void);
extern void TestOssAudio(void);
extern void TestDirectory(void);
extern void TestGpio(void);
extern void TestKeyInput(void);
extern void TestThreadPool(void);
extern void TestTuple(void);
extern void TestArray(void);
extern void TestConfig(void);
extern void TestLogger(void);
extern void TestTracer(void);
extern void TestRegExp(void);
extern void TestLocalTcpSocket(void);
extern void TestRtcFacade(void);
extern void TestTracerServer(void);
extern void TestHttpServer(void);
extern void TestFBScreen(void);
extern void TestImage(void);
extern void TestALSAAudio(void);



static void print_menu()
{
    TRACE_YELLOW("===================Test Menu List=================\n");
    TRACE_YELLOW("00--> Quick Test.           01--> MsgQueue Test.\n");
    TRACE_YELLOW("02--> Event Test.           03--> Thread Test.\n");
    TRACE_YELLOW("04--> Timer Test.           05--> TcpSocket Test.\n");
    TRACE_YELLOW("06--> UcpSocket Test.       07--> Utils Test.\n");
    TRACE_YELLOW("08--> CommandPipe Test.     09--> KeyInput Test.\n");
    TRACE_YELLOW("10--> KVProperty Test.      11--> ComPort Test.\n");
    TRACE_YELLOW("12--> Network Test.         13--> DataBase Test.\n");
    TRACE_YELLOW("14--> Task Test.            15--> HttpClient Test.\n");
    TRACE_YELLOW("16--> JSONData Test.        17--> CTimer Test.\n");
    TRACE_YELLOW("18--> TinyXml Test.         19--> StringFilter Test.\n");
    TRACE_YELLOW("20--> OSSAudio Test.        21--> Directory Test.\n");
	TRACE_YELLOW("22--> GPIO Test.            23--> ThreadPool Test.\n");
    TRACE_YELLOW("24--> Tuple Test.           25--> Array Test.\n");
    TRACE_YELLOW("26--> Config Test.          27--> Logger Test.\n");
    TRACE_YELLOW("28--> Tracer Test.          29--> RegExp Test.\n");
    TRACE_YELLOW("30--> LocalTcpSocket Test.  31--> RtcFacade Test.\n");
    TRACE_YELLOW("32--> TracerServer Test.    33--> HttpServer Test.\n");
    TRACE_YELLOW("34--> FrameBuffer Test.     35--> Image Test.\n");
    TRACE_YELLOW("36--> WavAudio Test(ALSA).                       \n");
    TRACE_YELLOW("-------------------------------------------------\n");
    TRACE_YELLOW("quit/q--> quit test program.\n");
    TRACE_YELLOW("==================================================\n");
}

void testMain()
{
    std::string inputStr;
    while (1)
    {
        print_menu();
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) return;
        else if ("00" == inputStr)	TestAny();
        else if ("01" == inputStr)	TestMsgQueue();
        else if ("02" == inputStr)	TestEvent();
        else if ("03" == inputStr)	TestThread();
        else if ("04" == inputStr)	TestTimer();
        else if ("05" == inputStr)	TestTcp();
        else if ("06" == inputStr)	TestUdp();
        else if ("07" == inputStr)	TestUtils();
        else if ("08" == inputStr)	TestCommandPipe();
        else if ("09" == inputStr)	TestKeyInput();
        else if ("10" == inputStr)	TestKVProperty();
        else if ("11" == inputStr)	TestComPort();
        else if ("12" == inputStr)  TestNetwork();
        //else if ("13" == inputStr)  TestDataBase();
        else if ("14" == inputStr)  TestTask();
        //else if ("15" == inputStr)  TestHttpClient();
        else if ("16" == inputStr)  TestJSON();
        else if ("17" == inputStr)  TestCTimer();
        //else if ("18" == inputStr)  TestXml();
        else if ("19" == inputStr)  TestStringFilter();
        else if ("20" == inputStr)  TestOssAudio();
        else if ("21" == inputStr)  TestDirectory();
		else if ("22" == inputStr)  TestGpio();
        else if ("23" == inputStr)  TestThreadPool();
        else if ("24" == inputStr)  TestTuple();
        else if ("25" == inputStr)  TestArray();
        else if ("26" == inputStr)  TestConfig();
        else if ("27" == inputStr)  TestLogger();
        else if ("28" == inputStr)  TestTracer();
        else if ("29" == inputStr)  TestRegExp();
        else if ("30" == inputStr)  TestLocalTcpSocket();
        else if ("31" == inputStr)  TestRtcFacade(); 
        else if ("32" == inputStr)  TestTracerServer();
        else if ("33" == inputStr)  TestHttpServer();
        else if ("34" == inputStr)  TestFBScreen();
        else if ("35" == inputStr)  TestImage();
        else if ("36" == inputStr)  TestALSAAudio();
        else
        {
            TRACE_RED("Unknown selection:%s !\n",inputStr.c_str());
        }
        inputStr.clear();
    }
}