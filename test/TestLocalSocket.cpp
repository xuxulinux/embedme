﻿#include "BaseType.h"
#include "Socket.h"
#include "Thread.h"
#include "Tracer.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

class MyLocalServer:public LocalTcpServer{
public:
    void taskHandleConnection(void* args)
    {
        LocalTcpSocket* connSocket =(LocalTcpSocket*)args;
        while(1)
        {
            char buf[64]={0};
            sint32 ret=connSocket->readData(buf, sizeof(buf)-1,500);
            if (ret>0)
            {
                TRACE_DBG_CLASS("server recv:[%s]\n",buf);
                char buf[]="hello local socket client!";
                connSocket->writeData(buf,sizeof(buf),500);  
            }
            else if (ret<0)
            {
                TRACE_ERR_CLASS("server recv error.\n");
                continue;
            }
            else/* 客户端已断开连接 */
            {
                break;
            }
        }
    }
    void onNewConnection(Socket* connSocket)
    {
        Thread::createTask(this,task_selector(MyLocalServer::taskHandleConnection),(void*)connSocket);
    }   
};

#define LOCAL_SOCKET_NAME   "my.localsocket"

void TestLocalTcpSocket(void)
{
	/* 启动服务线程 */
    MyLocalServer server;
    server.initServer(LOCAL_SOCKET_NAME);
    server.startServer();
	sleep(2);

	/* 客户端建立连接 */
	LocalTcpSocket client;
	client.open();
	client.connectLocal(LOCAL_SOCKET_NAME);
	while(1)
	{
		char sendBuf[]="hello local socket server!";
        TRACE_DBG("client send msg:%s\n",sendBuf);
		client.writeData(sendBuf, sizeof(sendBuf),-1);
		sleep(2);
        char buf[64]={0};
        int ret=client.readData(buf, sizeof(buf), 1000);
        if (ret>0)
        {
            TRACE_YELLOW("client get respond:[%s]\n",buf);
        }
        
	}
    server.stopServer();
}